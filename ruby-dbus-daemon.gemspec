Gem::Specification.new do |s|
  s.name = %q{ruby-dbus-daemon}
  s.version = "0.1.2"

  s.specification_version = 2 if s.respond_to? :specification_version=

  s.required_rubygems_version = Gem::Requirement.new(">= 0") if s.respond_to? :required_rubygems_version=
  s.authors = ["pangdudu"]
  s.date = %q{2009-07-26}
  s.default_executable = %q{ruby-dbus-daemon}
  s.description = %q{a small dbus-daemon wrapper for ruby.}
  s.email = %q{pangdudu@github}
  s.extra_rdoc_files = ["README.rdoc"]
  s.files = ["README.rdoc", "lib/dbus-daemon.rb"]
  s.has_rdoc = true
  s.homepage = %q{http://github.com/pangdudu/ruby-dbus-daemon}
  s.rubyforge_project = %q{http://github.com/pangdudu/ruby-dbus-daemon}
  s.require_paths = ["lib"]
  s.rubygems_version = %q{1.3.1}
  s.summary = %q{ride the ruby-dbus-daemon, jiiiihaaaa!}
end
