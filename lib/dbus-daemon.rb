=begin
  Ok, this is a really simple wrapper around the dbus-daemon console tool
  
  It's awfully dirty, but I need to concentrate on other stuff now :)
  
  Should become an app wrapping the .c into a nice .so
=end

class DbusDaemon
  attr_accessor :config_file,:output,:thread
  
  def initialize config_file #a dbus config file
    @daemon = `which dbus-daemon`.strip #search for the daemon
    @config_file = config_file
    #if the specified config file does not exist, break!
    unless File.exist? @config_file
      puts "ERROR: dbus-daemon: specified config-file: #{@config_file}, not readable."
      return nil
    end
    @cmd = "#{@daemon} --config-file=#{@config_file}"
    #no daemon found, sorry
    if @daemon.empty?
      puts "ERROR: dbus-daemon: no executable dbus-daemon found, sorry!"
      return nil
    end
  end


  #start the dbus-daemon
  def start_daemon
    got_twins = got_twin_daemon?
    unless got_twins
      #create a new thread for the daemon
      @thread = Thread.new do
        begin
          @output = `#{@cmd}`
        rescue
          puts "ERROR: dbus-daemon: dbus-daemon couldn't be started!"
          return nil  
        end
      end
      #just wait a really short time, so the console can answer
      sleep 0.1
      #ok, the output of the `` will be in @output
      puts "INFO: #{@cmd} is running!" if @output.nil? #so @output.nil? means, the daemon is running
      unless @output.nil?
        #@output.empty? = true means, we had an error, and it went to stderr
        puts "ERROR: dbus-daemon: dbus-daemon couldn't be started!" if @output.empty?
        #if there's something in @output (maybe we called --version), show it
        puts "INFO: dbus-daemon says: \n\t#{@output}" unless @output.empty?
      end
    end
    puts "WARNING: dbus-daemon: #{@cmd} not started, already seems to be running." if got_twins
  end
  
  #killing a ruby thread that spawned sth. on the console is not that easy to kill
  def stop_daemon
    @thread.kill
    #HACK, ok, this is not only dangerous, but hackish, we sould use ruby 1.9...
    pids_tokill = []
    ps = `ps x | grep "#{@cmd}"`.split("\n")
    ps.each {|p| pids_tokill << p.split("pts/")[0].to_i}
    #oki, pids_tokill now holds a list of pids, that we probably started, we'll kill them now
    pids_tokill.each {|p| `kill -9 #{p}`}
  end
  
  #check if another daemon doing the same is already running
  def got_twin_daemon?
    pids = []
    ps = `ps x | grep "#{@cmd}"`.split("\n")
    ps.each {|p| pids << p.split("pts/")[0].to_i unless p.include? "grep"}
    return false if pids.empty?
    #failsafe
    return true
  end
end

=begin
#for testing
dbd = DbusDaemon.new "somedbusconfigfile.conf" 
dbd.start_daemon
sleep 2
dbd.stop_daemon
=end
